//
//  CurrentToken.swift
//  XTNotification_Example
//
//  Created by 张晓鑫 on 2019/6/14.
//  Copyright © 2019 CocoaPods. All rights reserved.
//

import Foundation

public protocol CurrentAppProtocol {
    var info: [String:String] {get}
}
public protocol CurrentTokenProtocol {
    func isValidate() -> Bool
    func refreshToken(completion: @escaping (Bool, Error?) -> ())
}


open class CurrentAppManager {
    private var infoGetter: CurrentAppProtocol?
    private var tokenGetter: CurrentTokenProtocol?
    public static let shared = CurrentAppManager ()
    private init() {}
    public func set(_ infoGetter: CurrentAppProtocol) {
        self.infoGetter = infoGetter
    }
    public func set(_ tokenGetter: CurrentTokenProtocol) {
        self.tokenGetter = tokenGetter
    }
    public var info: [String : String]? {
        return infoGetter?.info
    }
    public var isTokenValidate: Bool? {
        return tokenGetter?.isValidate()
    }
    public func refreshToken(completion: @escaping (Bool, Error?)->() ) {
        tokenGetter?.refreshToken(completion: completion)
    }
}
