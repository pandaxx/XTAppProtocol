# XTAppProtocol

[![CI Status](https://img.shields.io/travis/wowzhangxiaoxin@gmail.com/XTAppProtocol.svg?style=flat)](https://travis-ci.org/wowzhangxiaoxin@gmail.com/XTAppProtocol)
[![Version](https://img.shields.io/cocoapods/v/XTAppProtocol.svg?style=flat)](https://cocoapods.org/pods/XTAppProtocol)
[![License](https://img.shields.io/cocoapods/l/XTAppProtocol.svg?style=flat)](https://cocoapods.org/pods/XTAppProtocol)
[![Platform](https://img.shields.io/cocoapods/p/XTAppProtocol.svg?style=flat)](https://cocoapods.org/pods/XTAppProtocol)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

XTAppProtocol is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'XTAppProtocol'
```

## Author

wowzhangxiaoxin@gmail.com, wowzhangxiaoxin@gmail.com

## License

XTAppProtocol is available under the MIT license. See the LICENSE file for more info.
